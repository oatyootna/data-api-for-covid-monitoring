**This is version 1.2 of Data API**
    
## To install
    
*  `sudo chmod +x setup.py`
*  `./setup.py`

## To run

*  `python3 DataCollector.py`


## Class Diagram

![Class Diagram](Class Diagram.png)


## Example of request.csv

| **Organization** | **Contact** | **Latitude** | **Longitude** |  **Post** | **Need** |
| ------ | ------ |------ |------  |------ |------ |
| รพ.ปัตตานี| 098-xx |10.8 | 101.3  | ขอรับบริจาคสิ่งของให้ รพ.ปัตตานี| {หน้ากาก N95,10},{แอลกอฮอล์,10}|