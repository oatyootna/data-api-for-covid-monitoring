import requests
import json
import os
import csv
import pandas as pd
import json
import re


class PostCollector:

    items_dictionary = {
        'หน้ากาก': 'หน้ากากอนามัย',
        'หน้ากากอนามัย': 'หน้ากากอนามัย',
        'mask': 'หน้ากากอนามัย',
        'แมส': 'หน้ากากอนามัย',
        'มาสก์': 'หน้ากากอนามัย',
        'ผ้ากันเปื้อน': 'ผ้ากันเปื้อน',
        'ผ้า': 'แบบ ผ้า',
        'n95': 'แบบ N95',
        'พลาสติก': 'แบบ พลาสติก',
        'สะท้อนน้ำ': 'แบบ สะท้อนน้ำ',
        'surgical': 'แบบ Surgical',
        'เยื่อกระดาษ': 'แบบ เยื่อกระดาษ',
        'ppe': 'ชุด PPE',
        'เจลแอลกอฮอล์': 'เจลแอลกอฮอล์',
        'เจล': 'เจลแอลกอฮอล์',
        'gel': 'เจลแอลกอฮอล์',
        'แอลกอฮอล์': 'แอลกอฮอล์',
        'alcohol': 'แอลกอฮอล์',
    }

    def get_dictionary(self):
        return self.items_dictionary

    # def item_checker(self):
    #     item_set = []

    #     df = pd.read_csv('prepared_posts_data.csv', encoding='utf-16')
    #     raw_items_data = df[['Items']]
    #     for item in raw_items_data:
    #         print(item)
        # checked_item = ''
        # for item in self.get_dictionary():
        #     if item in item_set:
        #         checked_item = checked_item + self.get_dictionary()[item]
        # print(checked_item+' : '+raw_item_data)

    # constructor
    def __init__(self, request_url):
        # find absolute path of this file & split by separate symbol
        path_list = str(os.path.abspath(__file__)).split(os.sep)
        # delete file"s name from path
        path_list = path_list[:len(path_list)-1]

        self.abs_path = str(os.sep).join(path_list) + \
            os.sep  # make absolute path

        # name of file that contain all post data.
        self.filename = self.abs_path + 'prepared_posts_data.csv'

        # URL for getting data from server.
        self.URL = request_url
        try:
            if os.path.isfile(self.abs_path + 'prepared_posts_data.csv'):
                os.remove(self.abs_path + 'prepared_posts_data.csv')
            # request data from URL.
            resp = requests.get(url=self.URL, verify=False).json()
            for i in range(len(resp['data'])):
                # prepare raw data to csv format.
                self.prep_data(resp['data'][i])
        except requests.exceptions.ConnectionError as error:
            print(error)

    # prepare a raw data to new format.
    def prep_data(self, post):
        purpose_data = post['ans']['purpose_message']
        # collect contact data.
        contact_data = ''
        for address in post['ans']['contact_address']:
            temp = contact_data
            contact_data = temp+' '+address['obj_text']
        # collect organize data.
        organize_data = ''
        for name in post['ans']['organize_name']:
            organize_data = organize_data+' '+name['obj_text']

        # init value of items data.
        items_data = []

        # collect all item.
        for item in post['ans']['items']:
            if self.check_duplicate_items(items_data, str.strip(str.lower(item['obj_text']))) is not True:
                # create json to store item detail.
                try:
                    itemJSON = {
                        "item": str.strip(str.lower(item['obj_text'])),
                        "amount": item['number_request']
                    }
                # if it's not have amount of item field.
                except KeyError:
                    itemJSON = {
                        "item": str.strip(str.lower(item['obj_text'])),
                        "amount": None
                    }
                # self.item_checker(str.strip(str.lower(item['obj_text'])))
                # self.item_set.add(str.strip(str.lower(item['obj_text'])))
                # add json data to items data.
                items_data.append(itemJSON)
            else:
                # print(organize_data+" has duplicate item.")
                ''

        # create file by using utf-16 encoder.
        with open(self.filename, 'a', newline='', encoding='utf-16') as data_file:
            # use ',' as delimiter to seperate each data field.
            data_writer = csv.writer(data_file, delimiter=',')
            # check if 'post.csv' is empty.
            if os.stat(self.filename).st_size == 0:
                # write an initial row to define a column name.
                column_names = ['Purpose', 'Organize',
                                'Contacts', 'Items', 'Last_Update', 'Raw_Post']
                data_writer.writerow(column_names)
                # write raw data to new csv format.
            data_writer.writerow([purpose_data, organize_data.strip(), contact_data.strip(
            ), items_data, post['ans_last_update'], post['post_text'].replace('\n', '')])
        data_file.close()

    # return all organize name.
    def get_organizes_name(self):
        df = pd.read_csv(self.filename, encoding='utf-16')
        # print(df)
        return df[['Organize']].values

    # return all raw post.
    def get_all_raw_post(self):
        df = pd.read_csv(self.filename, encoding='utf-16')
        return pd.DataFrame.to_records(df[['Raw_Post']])

    # return all raw post of specific organize.
    def get_raw_post_by_organize(self, organize):
        df = pd.read_csv(self.filename, encoding='utf-16')
        return df.loc[df.Organize == organize]['Raw_Post'].values[0]

    # return all items of specific organize.
    def get_items_by_organize(self, organize):
        df = pd.read_csv(self.filename, encoding='utf-16')
        try:
            item_str = df[df.Organize == organize][['Items']].values[0][0].replace('[', '').replace(
                ']', '').replace('\'', '\"').replace('{', '').replace('}', '').split(', ')
            item_str = str.join('', item_str)
            item_str = re.split('(\"item\":\s\"(.*?)\")', item_str)
          
            items_array = []
            for i in range(len(item_str)):
                if(i % 3 == 0) and i > 1:
                    # print('{'+item_str[i-2]+','+item_str[i]+'}')
                    items_array.append(json.dumps(
                        '{'+item_str[i-2]+','+item_str[i]+'}'))
            # for ele in items_array:
            #     print('S'+json.loads(ele))
            return(items_array)
        except IndexError:
            # print('E')
            return ''

    # return contact of specific organize.
    def get_contacts_by_organize(self, organize):
        df = pd.read_csv(self.filename, encoding='utf-16')
        try:
            return df[df.Organize == organize][['Contacts']].values[0][0]
        except IndexError:
            return ''

    # return all purpose from record.
    def get_all_purpose(self):
        df = pd.read_csv(self.filename, encoding='utf-16')
        all_purpose = []
        try:
            for value in df[['Purpose']].values:
                all_purpose.append(value[0])
            return all_purpose
        except IndexError:
            return ''

    # return type of purpose of specific organize.
    def get_purpose_by_organize(self, organize):
        df = pd.read_csv(self.filename, encoding='utf-16')
        try:
            return df[df.Organize == organize][['Purpose']].values[0][0]
        except IndexError:
            return ''

    # check if organzie has duplicate item.
    def check_duplicate_items(self, items, item):
        for existed_item in items:
            if item in existed_item['item']:
                return True
            else:
                return False


# if __name__ == "__main__":
    # a = PostCollector('')
    # print(a.get_all_purpose())
    # print(a.get_purpose_by_organize('รพ.ธรรมศาสตร์เฉลิมพระเกียรติ'))
    # print(a.get_items_by_organize(
        # 'โรงพยาบาลมหาราชนครศรีธรรมราช คุณแอน(พยาบาลห้องผ่าตัด)'))
    # print(a.get_items_by_organize('รพ.สต.โคกหนองแก'))
