import os
import json
import csv
from fuzzywuzzy import fuzz
import pandas as pd
import re


class ManagementInstance():

    def __init__(self, base_file_name):

        # find absolute path of this file & split by separate symbol
        path_list = str(os.path.abspath(__file__)).split(os.sep)
        # delete file"s name from path
        path_list = path_list[:len(path_list)-1]
        self.abs_path = str(os.sep).join(path_list) + \
            os.sep  # make absolute path

        # Actual hospital data
        self.df = pd.read_csv(self.abs_path + base_file_name)

        self.file_lists = [json_file for json_file in os.listdir(
            self.abs_path) if json_file.endswith('.json')]  # list every file that end with json

    def get_latitude_of(self, hospital_name):
        name = self.check_hospital_name_in_dataset(hospital_name)
        latitude = self.df.loc[self.df['ชื่อ'] == name]['Latitude'].values

        if(len(latitude) != 1):
            return 0
        else:
            return latitude[0]

    def get_longitude_of(self, hospital_name):
        name = self.check_hospital_name_in_dataset(hospital_name)
        latitude = self.df.loc[self.df['ชื่อ'] == name]['Longitude'].values

        if(len(latitude) != 1):
            return 0
        else:
            return latitude[0]

    def check_hospital_name_in_dataset(self, hospital_name):
        locationName = None
        dictionary = {'รพ.': 'โรงพยาบาล', 'โรงพยาบาล': 'โรงพยาบาล'}
        for key in dictionary:
            if key in hospital_name:
                start = re.search(key, hospital_name).start()
                end = hospital_name.find(' ', start) if hospital_name.find(
                    ' ', start) != -1 else len(hospital_name)
                locationName = hospital_name[start:end]
                locationName = re.sub(key, dictionary[key], locationName)
        filterinDataframe = self.df[self.df['ชื่อ'].str.contains(
            locationName)] if locationName else pd.DataFrame()
        if filterinDataframe.empty:
            return 'Not found the hospital name'
        else:
            hostpital_candidates = list(filterinDataframe['ชื่อ'].apply(lambda hospital_name: {
                                        'hospital_name': hospital_name, 'possibility': fuzz.token_set_ratio(hospital_name, locationName)}))

            if (len(hostpital_candidates) > 1):
                maxPossiblity = max(
                    hostpital_candidates, key=lambda hostpital_candidates: hostpital_candidates['possibility'])
                return maxPossiblity['hospital_name']
            else:
                return hostpital_candidates[0]['hospital_name']

    def write_data_into_csv(self):
        init_columns = ['Organization', 'Contact',
                        'Latitude', 'Longtitude', 'Quantity', 'Post']

        request_list = []

        for file_name in self.file_lists:
            path = str(self.abs_path + file_name)
            with open(path,  encoding="utf-8") as json_file:
                json_obj = json.load(json_file)
                purpose = str(json_obj['ans']['purpose_message']).lower()

                # request case
                if(purpose == 'request'):
                    csv_file = self.abs_path + 'request.csv'

                    # need to fix try problem after sent v1
                    try:
                        organization = json_obj['ans']['organize_name'][0]['text']
                    except KeyError as e:
                        if 'organize_name' in str(e):
                            organization = ''
                    try:
                        contact = json_obj['ans']['contact_address'][0]['text']
                    except KeyError as e:
                        if 'contact_address' in str(e):
                            contact = ''
                    try:
                        post = json_obj['post_text']
                    except KeyError as e:
                        if 'post_text' in str(e):
                            post = ''
                    try:

                        items = json_obj['ans']['items']
                        item_message = ""

                        for item in items:
                            item_message = item_message + \
                                '{'+str(item["text"]) + ':' + \
                                str(item["number_request"])+'},'
                        item_message = item_message[:len(item_message)-1]
                    except KeyError as e:
                        if 'ans' in str(e):
                            items = ''

                    csv_format_data = {
                        'Organization': organization,
                        'Contact': contact,
                        'Latitude':  self.get_latitude_of(organization),
                        'Longtitude': self.get_longitude_of(organization),
                        'Quantity': None,
                        'Post': post,
                        'Need': item_message
                    }
                    request_list.append(csv_format_data)

        try:
            with open(csv_file, "w", newline="",  encoding='utf-16') as csvfile:
                init_columns.append('Need')
                writer = csv.DictWriter(
                    csvfile, fieldnames=init_columns,delimiter='\t')
                writer.writeheader()
                for data in request_list:
                    writer.writerow(data)
               
                csvfile.close()
                print("success")
        except IOError:
            print("I/O error")


if __name__ == "__main__":
    i = ManagementInstance('Hospital_data.csv')
    i.write_data_into_csv()
