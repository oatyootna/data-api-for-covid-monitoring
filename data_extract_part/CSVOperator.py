import os
import json
import csv
from fuzzywuzzy import fuzz
import pandas as pd
import re


class CSVOperatorInstance():
    def __init__(self, column_titles, prepared_data, export_name):
        # find absolute path of this file & split by separate symbol
        path_list = str(os.path.abspath(__file__)).split(os.sep)
        # delete file"s name from path
        path_list = path_list[:len(path_list)-1]

        self.abs_path = str(os.sep).join(path_list) + \
            os.sep  # make absolute path

        self.prepared_data = prepared_data
        self.export_name = export_name
        self.data_columns = column_titles

        self.export_path = self.abs_path + export_name

    def write_data_into_csv(self):
       
        try:
            with open( self.export_path, "w", newline="",  encoding='utf-16') as csvfile:
               
                writer = csv.DictWriter(
                    csvfile, fieldnames=self.data_columns, delimiter='\t')
                writer.writeheader()
                for data in  self.prepared_data:
                    writer.writerow(data)

                csvfile.close()
                # print(str(self.data_columns) + " file writing has"+ " successed")
        except IOError:
            print("I/O error")
