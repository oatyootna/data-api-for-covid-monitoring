import os
import json
import csv
from fuzzywuzzy import fuzz
import pandas as pd
import re


class HospitalCollector():
    def __init__(self, hospital_information_file):
        # find absolute path of this file & split by separate symbol
        path_list = str(os.path.abspath(__file__)).split(os.sep)
        # delete file"s name from path
        path_list = path_list[:len(path_list)-1]
        
        self.abs_path = str(os.sep).join(path_list) + \
            os.sep  # make absolute path
        # Actual hospital data
        self.df = pd.read_csv(self.abs_path + hospital_information_file)
    
    def get_latitude_of(self, hospital_name):
        name = self.check_hospital_name_in_dataset(hospital_name)
        latitude = self.df.loc[self.df['ชื่อ'] == name]['Latitude'].values

        if(len(latitude) != 1):
            return 0
        else:
            return latitude[0]

    def get_longitude_of(self, hospital_name):
        name = self.check_hospital_name_in_dataset(hospital_name)
        latitude = self.df.loc[self.df['ชื่อ'] == name]['Longitude'].values

        if(len(latitude) != 1):
            return 0
        else:
            return latitude[0]

    def get_size_of(self, hospital_name):
        
        name = self.check_hospital_name_in_dataset(hospital_name)
        beds = self.df.loc[self.df['ชื่อ'] == name]['จำนวนเตียงตามจริง'].values

        if(len(beds) != 1):
            return 0
        else:
            return beds[0]

    def check_hospital_name_in_dataset(self, hospital_name):
        locationName = None
        dictionary = {'รพ.': 'โรงพยาบาล', 'โรงพยาบาล': 'โรงพยาบาล', 'รพ ': "โรงพยาบาล"}
        try :
            for key in dictionary:
                if key in hospital_name:
                    start = re.search(key, hospital_name).start()
                    end = hospital_name.find(' ', start + len(key)) if hospital_name.find(
                        ' ', start + len(key) ) != -1 else len(hospital_name)
                    locationName = hospital_name[start:end]
                    locationName = re.sub(key, dictionary[key], locationName)
            filterinDataframe = self.df[self.df['ชื่อ'].str.contains(
                locationName)] if locationName else pd.DataFrame()

        
            if filterinDataframe.empty:
                return hospital_name
            else:
                hostpital_candidates = list(filterinDataframe['ชื่อ'].apply(lambda hospital_name: {
                                            'hospital_name': hospital_name, 'possibility': fuzz.token_set_ratio(hospital_name, locationName)}))

                if (len(hostpital_candidates) > 1):
                    maxPossiblity = max(
                        hostpital_candidates, key=lambda hostpital_candidates: hostpital_candidates['possibility'])
                    return maxPossiblity['hospital_name']
                else:
                    return hostpital_candidates[0]['hospital_name']
        except:
            return hospital_name