from HospitalCollector import HospitalCollector
from PostCollector import PostCollector
from RequestPostInstance import RequestPostInstance
from ResponsePostInstance import ResponsePostInstance
from SupplyPostInstance import SupplyPostInstance
from CSVOperator import CSVOperatorInstance
from config import PURPOSE_CONFIG, COLLECTOR_CONFIG
from collections import namedtuple

# TO DO
# - Check that organizes name are correct
# - handling the error

class DataController():

    base_file_name = COLLECTOR_CONFIG["HOSPITAL_BASE_DATA"]
    request_url = COLLECTOR_CONFIG["POST_URL"]

    Constants = namedtuple('Constants', ["REQUEST", "SUPPLY", "RESPONSE"])
    purpose_constants = Constants("REQUEST", "SUPPLY", "RESPONSE")

    def __init__(self):
        self.hospital_collector = HospitalCollector(self.base_file_name)
        self.post_collector = PostCollector(self.request_url)

        self.request_data_list = list()
        self.supply_data_list = list()
        self.response_data_list = list()

        self.request_column_title = list()
        self.response_column_title = list()
        self.supply_column_title = list()

    def prepare_data(self):

        init_data = list(dict())
        organizes = self.post_collector.get_organizes_name()
        post_size = len(organizes)

        
        print('processing the data ' + str(post_size) + ' rows  ... \n (might take a long time if there are a lot of data) ')
        
        for index in range(post_size):

            organize_name = str(organizes[index][0])
            purpose = self.post_collector.get_purpose_by_organize(
                organize_name).upper()
            print(str(index) +" " + organize_name)
            if purpose == self.purpose_constants.REQUEST:

                self.request_obj = RequestPostInstance(
                    organize_name, self.post_collector, self.hospital_collector)

                self.request_column_title = self.request_obj.get_columns_title()

                self.request_data_list.append(
                    self.request_obj.get_prepared_dict())

            elif purpose == self.purpose_constants.SUPPLY:

                self.supply_obj = SupplyPostInstance(
                    organize_name, self.post_collector, self.hospital_collector)
                
                self.supply_column_title = self.supply_obj.get_columns_title()

                self.supply_data_list.append(
                    self.supply_obj.get_prepared_dict())

            elif purpose == self.purpose_constants.RESPONSE:
                
                self.response_obj = ResponsePostInstance(
                    organize_name, self.post_collector, self.hospital_collector)
                
                self.response_column_title = self.response_obj.get_columns_title()

                self.response_data_list.append(
                    self.response_obj.get_prepared_dict())


        print('processing data successed ... ')

    def write_prepared_data(self, purpose_title):

        prepared_data = dict()
        file_name = None
        columns = list()
        print("=============================================")
        print( "Start writing " + purpose_title + " file")
        
        if purpose_title == self.purpose_constants.REQUEST:

            columns = self.request_column_title
            prepared_data = self.request_data_list
            file_name = PURPOSE_CONFIG[purpose_title]["OUTPUT_FILE"]

        elif purpose_title == self.purpose_constants.RESPONSE:
            
            columns = self.response_column_title
            prepared_data = self.response_data_list
            file_name = PURPOSE_CONFIG[purpose_title]["OUTPUT_FILE"]

        elif purpose_title == self.purpose_constants.SUPPLY:
            
            columns = self.supply_column_title
            prepared_data = self.supply_data_list
            file_name = PURPOSE_CONFIG[purpose_title]["OUTPUT_FILE"]

        csv_writer = CSVOperatorInstance(columns,
                                         prepared_data, file_name)
        csv_writer.write_data_into_csv()

        print(purpose_title + " file writing has succesed")
        print("=============================================\n")


if __name__ == "__main__":
    d = DataController()
    d.prepare_data()
    d.write_prepared_data("REQUEST")
    d.write_prepared_data("SUPPLY")
    d.write_prepared_data("RESPONSE")
