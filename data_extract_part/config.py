COLLECTOR_CONFIG = {
    "HOSPITAL_BASE_DATA": "HospitalsTH_Updated04Apr.csv",
    "POST_URL": "https://tonkitlab.net/backend/getAllAnswers?key=793033db97268fc9ceebde269797e54b&fbclid=IwAR20kKJVtxla5Pw5Iy3JeGsn_vJcjMXl2g7tI_zeomrXiMzdzyLZfgiMMtI"
}

PURPOSE_CONFIG = {
    "REQUEST": {
        "OUTPUT_FILE": "request.csv"
    },
    "RESPONSE": {
        "OUTPUT_FILE": "response.csv"
    },
    "SUPPLY": {
        "OUTPUT_FILE": "supply.csv"
    }
}
