import json
import ast

class RequestPostInstance():

    prepare_dict = {
        'Purpose' : "",
        'Organization': "",
        'Contact': "",
        'Latitude':  0,
        'Longitude': 0,
        'Post': "",
        'Need' : ""
    }

    def __init__(self, organize_name, post_collector, hospital_collector):

        self.checked_org_name = str(
            hospital_collector.check_hospital_name_in_dataset(organize_name))
        self.longitude = hospital_collector.get_longitude_of(
            self.checked_org_name)
        self.latitude = hospital_collector.get_latitude_of(
            self.checked_org_name)

        self.raw_post = post_collector.get_raw_post_by_organize(organize_name)

        self.contact = post_collector.get_contacts_by_organize(organize_name)
        
        items = post_collector.get_items_by_organize(organize_name)
        self.item_str_format = ""
                
        for item in items:
            obj = json.loads(item)
            item_obj = ast.literal_eval(obj)
            amount = " N/A"
            if item_obj['amount'] != None:
                amount = " "+ str(item_obj['amount'])
            self.item_str_format = self.item_str_format + str(item_obj['item']) + amount + ", "
        self.item_str_format =  self.item_str_format[:len(self.item_str_format)-2]
        
        self.prepare_dict = {
                    'Purpose' : "Request",
                    'Organization': self.checked_org_name,
                    'Contact': self.contact,
                    'Latitude':  self.latitude,
                    'Longitude': self.longitude,
                    'Post': self.raw_post,
                    'Need' : self.item_str_format
        }

    def get_prepared_dict(self):
       return self.prepare_dict

    def get_columns_title(self):
        return self.prepare_dict.keys()